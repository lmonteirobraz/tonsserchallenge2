package com.lucasmbraz.tonsserchallenge

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.lucasmbraz.tonsserchallenge.model.User
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_item_follower.view.*
import org.w3c.dom.Text
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModel: FollowersViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        viewModel.viewState().observe(this, Observer<FollowersViewState>{ state ->
            if (state != null) {
                when {
                    state.isLoading -> showProgressIndicator()
                    state.error != null -> showSadPath(state.error)
                    else -> showRecyclerView(state.followers)
                }
            }
        })
    }

    private fun showProgressIndicator() {
        progress.visibility = VISIBLE
        sadPath.visibility = GONE
        recyclerView.visibility = GONE
    }

    private fun showSadPath(errorMessage: String) {
        sadPath.visibility = VISIBLE
        sadPath.text = errorMessage

        progress.visibility = GONE
        recyclerView.visibility = GONE
    }

    private fun showRecyclerView(followers: List<User>) {
        progress.visibility = GONE
        sadPath.visibility = GONE

        recyclerView.apply {
            visibility = VISIBLE
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = FollowersAdapter(followers)
        }
    }
}

class FollowersAdapter(private val followers: List<User>) : RecyclerView.Adapter<FollowersAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView by lazy { view.name }
        val image: ImageView by lazy { view.image }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_follower, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = followers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val follower = followers[position]
        val context = holder.view.context
        holder.name.text = context.getString(R.string.follower_name, follower.firstName, follower.lastName)
        Glide.with(context).load(follower.picture).into(holder.image)
    }
}