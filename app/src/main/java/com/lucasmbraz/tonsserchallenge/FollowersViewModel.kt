package com.lucasmbraz.tonsserchallenge

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.ViewModel
import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.repository.FollowersRepository
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

data class FollowersViewState(
    val followers: List<User> = emptyList(),
    val isLoading: Boolean = false,
    val error: String? = null
)

class FollowersViewModel(repository: FollowersRepository) : ViewModel() {
    private val fetcher = repository.getFollowers()
            .flatMap { followers ->
                if (followers.isNotEmpty()) {
                    Observable.just(FollowersViewState(followers = followers))
                } else {
                    Observable.just(FollowersViewState(error = "You don't have any followers yet."))
                }
            }
            .onErrorReturn { FollowersViewState(error = "Something wrong happened") }
            .startWith(FollowersViewState(isLoading = true))

    private val subject = BehaviorSubject.create<FollowersViewState>()

    init {
        fetcher.subscribe(subject)
    }

    // Output
    fun viewState(): LiveData<FollowersViewState> = LiveDataReactiveStreams.fromPublisher(subject.toFlowable(BackpressureStrategy.LATEST))
}