package com.lucasmbraz.tonsserchallenge.di

import com.lucasmbraz.tonsserchallenge.network.TonsserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides @Singleton
    fun provideRetrofit() = Retrofit.Builder()
            .baseUrl("http://staging-api.tonsser.com")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides @Singleton
    fun provideTonsserApi(retrofit: Retrofit) = retrofit.create(TonsserApi::class.java)
}