package com.lucasmbraz.tonsserchallenge.di

import com.lucasmbraz.tonsserchallenge.repository.FollowersRepository
import com.lucasmbraz.tonsserchallenge.repository.ProductionFollowersRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {
    @Binds
    fun provideFollowersRepository(repository: ProductionFollowersRepository): FollowersRepository
}