package com.lucasmbraz.tonsserchallenge.di

import com.lucasmbraz.tonsserchallenge.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ActivityBuilderModule::class,
    RepositoryModule::class,
    NetworkModule::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(app: App): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)
}