package com.lucasmbraz.tonsserchallenge.di

import com.lucasmbraz.tonsserchallenge.MainActivity
import com.lucasmbraz.tonsserchallenge.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuilderModule {
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun bindMainActivity(): MainActivity
}