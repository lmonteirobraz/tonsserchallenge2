package com.lucasmbraz.tonsserchallenge.network

import com.lucasmbraz.tonsserchallenge.model.User
import io.reactivex.Observable
import retrofit2.http.GET

interface TonsserApi {
    @GET("51/users/christian-planck/followers")
    fun getFollowers(): Observable<FollowersResponse>
}

data class FollowersResponse(val response: List<User>)