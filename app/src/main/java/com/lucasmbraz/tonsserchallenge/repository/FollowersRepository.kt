package com.lucasmbraz.tonsserchallenge.repository

import com.lucasmbraz.tonsserchallenge.model.User
import com.lucasmbraz.tonsserchallenge.network.TonsserApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

interface FollowersRepository {
    fun getFollowers(): Observable<List<User>>
}

class ProductionFollowersRepository @Inject constructor(private val tonsserApi: TonsserApi) : FollowersRepository {
    override fun getFollowers(): Observable<List<User>> {
        return tonsserApi.getFollowers()
                .map { it.response }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}