package com.lucasmbraz.tonsserchallenge

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import com.lucasmbraz.tonsserchallenge.repository.FollowersRepository
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideViewModel(activity: MainActivity, repository: FollowersRepository): FollowersViewModel
            = ViewModelProviders.of(activity, FollowersViewModelFactory(repository)).get(FollowersViewModel::class.java)
}

class FollowersViewModelFactory(private val repository: FollowersRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FollowersViewModel(repository) as T
    }
}