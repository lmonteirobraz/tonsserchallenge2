package com.lucasmbraz.tonsserchallenge.assertions

import android.view.View

import com.google.common.truth.FailureMetadata
import com.google.common.truth.Subject

import com.google.common.truth.Fact.simpleFact
import com.google.common.truth.Truth.assertAbout

class ViewSubject private constructor(metadata: FailureMetadata, actual: View) : Subject<ViewSubject, View>(metadata, actual) {

    fun isVisible() {
        if (actual().visibility != View.VISIBLE) {
            failWithActual(simpleFact("expected to be VISIBLE"))
        }
    }

    fun isNotVisible() {
        if (actual().visibility == View.VISIBLE) {
            failWithActual(simpleFact("expected to NOT be VISIBLE"))
        }
    }

    companion object {
        private val VIEW_SUBJECT_FACTORY = Subject.Factory<ViewSubject, View> { metadata, actual -> ViewSubject(metadata, actual) }

        fun views(): Subject.Factory<ViewSubject, View> {
            return VIEW_SUBJECT_FACTORY
        }

        fun assertThat(view: View): ViewSubject {
            return assertAbout(VIEW_SUBJECT_FACTORY).that(view)
        }
    }
}
