package com.lucasmbraz.tonsserchallenge

import android.os.Build.VERSION_CODES.O
import com.lucasmbraz.tonsserchallenge.assertions.ViewSubject.Companion.assertThat
import com.google.common.truth.Truth.assertThat
import kotlinx.android.synthetic.main.activity_main.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

/**
 * Unit tests for [MainActivity].
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [O])
class MainActivityTest {

    private lateinit var activity: MainActivity

    @Before
    fun setUp() {
        activity = Robolectric.setupActivity(MainActivity::class.java)
    }

    @Test @Ignore
    fun showsSadPath_whenThereAreNoFollowers() {
        assertThat(activity.sadPath).isVisible()
        assertThat(activity.sadPath.text).isEqualTo("There are no followers.")
    }

    @Test @Ignore
    fun showsProgressIndicator_whileLoadingData() {
        assertThat(activity.progress).isVisible()
        assertThat(activity.followers).isNotVisible()
        assertThat(activity.sadPath).isNotVisible()
    }
}